package com.natanloterio.movieapp;

import com.natanloterio.movieapp.data.FakeGenresServiceApiImpl;
import com.natanloterio.movieapp.data.GenresRepository;
import com.natanloterio.movieapp.data.InMemoryGenresRepository;
import com.natanloterio.movieapp.movies.FakeMoviesServiceApiImpl;
import com.natanloterio.movieapp.movies.InMemoryMoviesRepository;
import com.natanloterio.movieapp.movie.list.MoviesRepository;

/**
 * Created by natanloterio on 03/08/2016.
 */
public class Injection {


    public static GenresRepository providesGenreRepository(){
        return new InMemoryGenresRepository(new FakeGenresServiceApiImpl());
    }

    public static MoviesRepository providesMoviesRepository() {
        return new InMemoryMoviesRepository(new FakeMoviesServiceApiImpl());
    }
}
