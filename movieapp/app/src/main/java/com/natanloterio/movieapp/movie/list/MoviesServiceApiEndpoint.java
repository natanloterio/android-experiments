package com.natanloterio.movieapp.movie.list;

import android.content.Context;

import com.natanloterio.movieapp.R;
import com.natanloterio.movieapp.data.Movie;
import com.natanloterio.movieapp.data.MovieAPI;
import com.natanloterio.movieapp.data.MovieTransport;
import com.natanloterio.movieapp.data.MovieRESTApi;

import java.util.List;

import retrofit2.Call;

/**
 * Created by natanloterio on 08/08/2016.
 */
public class MoviesServiceApiEndpoint {

    private final Context context;

    public MoviesServiceApiEndpoint(Context applicationContext){
        this.context = applicationContext.getApplicationContext();
    }

    public List<Movie> loadMovies(int page) throws java.io.IOException {

        MovieRESTApi service = MovieAPI.getInstance(context);

        Call<MovieTransport> response = service.orderByRate(context.getString(R.string.MOVIE_DB_API_KEY), page);
        MovieTransport movieTransport = response.execute().body();

        return movieTransport.getResults();
    }

}
