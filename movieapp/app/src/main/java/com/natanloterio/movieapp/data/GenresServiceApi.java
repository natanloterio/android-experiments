package com.natanloterio.movieapp.data;

import java.util.List;

/**
 * Created by natanloterio on 03/08/2016.
 */
public interface GenresServiceApi {

    interface GenresServiceCallback<T>{
        void onLoaded(T load);
    }

    void getGenres(GenresServiceCallback<List<Genre>> callback);

    void saveGenreAsFavorite(Genre genre);

    void removeGenreAsFavorite(Genre genre);
}
