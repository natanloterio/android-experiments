package com.natanloterio.movieapp.movie.list;

import com.natanloterio.movieapp.data.Movie;

import java.util.List;

/**
 * Created by natanloterio on 08/08/2016.
 */
public interface MoviesServiceApi {

    interface MoviesServiceCallback<T> {
        void onLoaded(T load, int currentPage, int maxPage);
        void onError(Throwable e);
    }

    void getMovies(int page, MoviesServiceCallback<List<Movie>> callback);
}