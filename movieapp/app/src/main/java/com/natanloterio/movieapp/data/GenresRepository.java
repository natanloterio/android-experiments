package com.natanloterio.movieapp.data;

import java.util.List;

/**
 * Created by natanloterio on 03/08/2016.
 */
public interface GenresRepository {

    interface LoadGenresCallback{
        void onLoadGenres(List<Genre> genres);
    }

    void getAllGenres(LoadGenresCallback callback);
    void saveGenreAsFavorite(Genre genre);
    void removeGenreAsFavorite(Genre genre);
}
