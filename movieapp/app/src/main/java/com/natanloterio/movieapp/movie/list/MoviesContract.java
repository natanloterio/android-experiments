package com.natanloterio.movieapp.movie.list;

import android.support.annotation.NonNull;

import com.natanloterio.movieapp.data.Movie;

import java.util.List;

/**
 * Created by natanloterio on 08/08/2016.
 */
public class MoviesContract {

    interface View{
        void showActivityIndicator();
        void hideActivityIndicator();
        void showLazyLoad();
        void hideLazyLoad();
        void showMovies(List<Movie> movies, int currentPage, int maxPage);
        void showMovieDetailUi(Integer movieId);
        void showError(Throwable e);

        void appendPage(List<Movie> movies, int currentPage);
    }

    interface UserActionsListener{
        void loadMovies(boolean forceUpdate);
        void loadPage(int page);
        void openMovieDetail(@NonNull Movie movie);
    }
}
