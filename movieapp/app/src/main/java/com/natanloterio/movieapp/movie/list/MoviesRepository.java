package com.natanloterio.movieapp.movie.list;

import com.natanloterio.movieapp.data.Movie;

import java.util.List;

/**
 * Created by natanloterio on 08/08/2016.
 */
public interface MoviesRepository {

    interface LoadMoviesCallback {
        void onLoadMovies(List<Movie> movies, int currentPage, int maxPage);
        void onErrorLoadingMovies(Throwable e);
    }

    void getMovies(int pageIndex, LoadMoviesCallback callback);
}
